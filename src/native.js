import './styles.css'

const counter = document.getElementById('counter')
const addBtn = document.getElementById('add')
const incrimentBtn = document.getElementById('sub')
const async = document.getElementById('async')
const theme = document.getElementById('theme')

let state = 0;

addBtn.onclick = () => {
  console.log('added')
  render(state++)
}

// addBtn.addEventListener('click', () => {
//   state++
// })

incrimentBtn.addEventListener('click', () => {
  console.log('ssss');
  state--
  render()
})


async.addEventListener('click', () => {
  setTimeout(() => {
    state++
    render()
    alert('added counter?')
  }, 1000)
})

theme.addEventListener('click', () => {
  document.body.classList.toggle('dark')
})

function render() {
  counter.textContent = state.toString()
}

render()
