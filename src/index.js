import './styles.css'
import { crateStore } from './crateStore'
import { rootReducer } from './redux/rootReducer'

const counter = document.getElementById('counter')
const addBtn = document.getElementById('add')
const dicrimentBtn = document.getElementById('sub')
const async = document.getElementById('async')
const theme = document.getElementById('theme')

const store = crateStore(rootReducer, 0)


addBtn.addEventListener('click', (e) => {
  store.dispath({ type: 'INCREMENT' })
  console.log(e);
})

dicrimentBtn.addEventListener('click', () => {
  store.dispath({ type: 'DECRIMENT' })
})

store.subscribe(() => {
  const state = store.getState()

  counter.textContent = state
})

store.dispath({ type: 'INIT_APPLICATION' })
