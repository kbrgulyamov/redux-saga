export function crateStore(rootReducer, initialStore) {
  let state = rootReducer(initialStore, { type: '__INIT__' })
  const subscribers = []
  return {
    dispath(action) {
      state = rootReducer(state, action)
      subscribers.forEach(sub => sub())
    },
    subscribe(callback) {
      subscribers.push(callback)
    },
    getState() {
      return state
    }
  }
}